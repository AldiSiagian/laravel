@extends('adminlte.master')

@section('content')
<br>
<div class="ml-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @foreach($pertanyaan as $p)
        <form role="form" method="post" action="/sanbercode/public/pertanyaan/{{ $p->id }}">
        @csrf
        @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{ $p->judul }}" required>
                </div>
                <div class="form-group">
                    <label for="body">Isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{ $p->isi }}" required>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Ubah Pertanyaan</button>
            </div>
        </form>
        @endforeach
    </div>
</div>
<br>
@endsection
