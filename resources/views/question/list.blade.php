@extends('adminlte.master')

@section('content')
<br>
<div class="ml-3 mr-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="text-right">
                <a href="{{ route('create') }}">+Tambah Pertanyaan</a>
            </div>

            <table class="table table-bordered">
                <thead>                  
                    <tr>
                        <th class="text-center" style="width: 0.1%;">No.</th>
                        <th style="width: 6%;">Judul</th>
                        <th style="width: 10%;">Isi</th>
                        <th style="width: 3%;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($pertanyaan as $key => $p)
                    <tr>
                        <td class="text-center">{{ $key + 1 }}</td>
                        <td>{{ $p->judul }}</td>
                        <td>{{ $p->isi }}</td>
                        <td class="text-center">
                            <a href="pertanyaan/{{ $p->id }}/edit" class="btn btn-app"><i class="fas fa-edit"></i> Edit</a>
                            <a href="pertanyaan/{{ $p->id }}" class="btn btn-app"><i class="fas fa-eye"></i> Detail</a>
                            <form role="form" method="post" action="{{ route('question.destroy', $p->id) }}">
                            @csrf
                            @method('DELETE')
                                <button type="submit" class="btn btn-app"><i class="fas fa-trash"></i> Delete</button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" class="text-center">No question added yet.</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
<br>
@endsection
