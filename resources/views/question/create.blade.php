@extends('adminlte.master')

@section('content')
<br>
<div class="ml-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Tambah Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" method="post" action="{{ route('store') }}">
        @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukkan Judul Pertanyaan" required>
                </div>
                <div class="form-group">
                    <label for="body">Isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" placeholder="Masukkan Isi Pertanyaan" required>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Tambahkan</button>
            </div>
        </form>
    </div>
</div>
<br>
@endsection
