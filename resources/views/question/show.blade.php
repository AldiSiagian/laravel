@extends('adminlte.master')

@section('content')
<br>
<div class="ml-3 mr-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Detail Pertanyaan</h3>
        </div>
        
        <!-- /.card-header -->
        <div class="card-body">
        @foreach($pertanyaan as $p)
            <form>
            @csrf
                <table class="table table-bordered">
                    <tr>
                        <th style="width:200px;">Pembuat Pertanyaan</th>
                        <th class="text-center" style="width:40px;">:</th>
                        <td>{{ $p->profil_id }}</td>
                    </tr>
                    <tr>
                        <th style="width:200px;">Judul Pertanyaan</th>
                        <th class="text-center" style="width:40px;">:</th>
                        <td>{{ $p->judul }}</td>
                    </tr>
                    <tr>
                        <th style="width:200px;">Isi Pertanyaan</th>
                        <th class="text-center" style="width:40px;">:</th>
                        <td>{{ $p->isi }}</td>
                    </tr>
                    <tr>
                        <th style="width:200px;">Tanggal Dibuat</th>
                        <th class="text-center" style="width:40px;">:</th>
                        <td>{{ $p->tanggal_dibuat }}</td>
                    </tr>
                    <tr>
                        <th style="width:200px;">Tanggal Diperbaharui</th>
                        <th class="text-center" style="width:40px;">:</th>
                        <td>{{ $p->tanggal_diperbaharui }}</td>
                    </tr>
                    <tr>
                        <th style="width:200px;">Jawaban Paling Tepat</th>
                        <th class="text-center" style="width:40px;">:</th>
                        <td>{{ $p->jawaban_tepat_id }}</td>
                    </tr>
                </table>
            </form>
            @endforeach
            <div class="text-right">
                <a href="{{ route('base') }}" class="btn btn-warning"><i class="fas fa-arrow-left"></i> Kembali</a>
            </div>
        </div>
    </div>
</div>
<br>
@endsection
